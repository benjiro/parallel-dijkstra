import com.sun.org.apache.xpath.internal.operations.Bool;

import java.util.HashMap;
import java.util.Map;

public class Vertex {

    private final Integer id;
    private Map<Vertex, Integer> neighboursWithDistances = new HashMap<>();
    private Integer distance = Integer.MAX_VALUE;
    private Boolean visited = false;

    public Vertex(Integer id) {
        this.id = id;
    }

    public void addNeighbour(Vertex destination, Integer distance) {
        neighboursWithDistances.put(destination, distance);
    }

    public Integer getId(){
        return id;
    }

    public Map<Vertex, Integer> getNeighboursWithDistances() {
        return neighboursWithDistances;
    }

    public Boolean getVisited() {
        return visited;
    }

    public void setVisited(Boolean visited) {
        this.visited = visited;
    }
}
