import java.util.*;
import java.util.stream.IntStream;

public class Main {

    public static void main(String[] args) {
        Vertex[] vertex = new Vertex[5000];
        for (int i = 0; i < 5000; i++) {
            vertex[i] = new Vertex(i);
        }
        for (int i = 0; i < 5000; i++) {
            Random random = new Random();
            int nEdges = random.nextInt(100);
            for (int j = 0; j < nEdges; j++) {
                int vertexId = random.nextInt(5000);
                int distance = random.nextInt(50) + 1;
                vertex[i].addNeighbour(vertex[vertexId], distance);
            }
        }

       /* Vertex a = new Vertex(0);
        Vertex b = new Vertex(1);
        Vertex c = new Vertex(2);
        Vertex d = new Vertex(3);
        Vertex e = new Vertex(4);
        Vertex f = new Vertex(5);
        Vertex g = new Vertex(6);
        Vertex h = new Vertex(7);
        a.addNeighbour(b, 2);
        a.addNeighbour(e, 5);
        a.addNeighbour(g, 7);
        b.addNeighbour(f, 3);
        c.addNeighbour(d, 1);
        d.addNeighbour(h, 1);
        e.addNeighbour(c, 2);
        f.addNeighbour(e, 3);
        f.addNeighbour(h, 10);
        g.addNeighbour(e, 1);*/

        Set<Vertex> setOfVertices = Utils.setOfAddedObjects(vertex);
        Dijkstra dijkstra = new Dijkstra(0, 80, setOfVertices);

        long startTime = System.currentTimeMillis();
        Integer distance = dijkstra.executeSerialAlgorithm();
        long estimatedTime = System.currentTimeMillis() - startTime;

        System.out.println(estimatedTime + "ms");

    }


}
