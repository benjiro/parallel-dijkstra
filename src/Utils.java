import java.util.*;

public class Utils {

    private Utils() {}

    public static <T> Set<T> setOfAddedObjects(T... t) {
        return new HashSet<>(Arrays.asList(t));
    }
}
