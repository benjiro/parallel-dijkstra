import java.util.*;
import java.util.stream.Collectors;

public class Dijkstra {

    private Set<Vertex> vertices;
    private Set<Vertex> settledNodes;
    private Set<Vertex> unSettledNodes;
    private Map<Vertex, Integer> distance;
    private Integer start;
    private Integer stop;
    private int[] d;
    private int[] p;

    public Dijkstra(Integer start, Integer stop, Set<Vertex> allVertices) {
        this.start = start;
        this.stop = stop;
        this.unSettledNodes = allVertices;
    }

    public Integer executeSerialAlgorithm() {
        initializeVariables();

        while(settledNodes.size() != vertices.size()) {
            int position = 0;
            int min = Integer.MAX_VALUE;
            boolean flag;
            Vertex chosenVertex = null;
            for (Integer j = 0; j < d.length; j++) {
                flag = true;
                if (d[j] < min) {
                    final Integer index = j;
                    Vertex v = unSettledNodes.parallelStream()
                            .filter(s -> s.getId().equals(index))
                            .findFirst()
                            .orElse(null);
                    if (Objects.equals(v, null)) {
                        flag = false;
                    } else if (v.getVisited())
                        flag = false;

                    if (flag) {
                        min = d[j];
                        position = j;
                        chosenVertex = v;
                    }
                }
            }

            if (!Objects.equals(chosenVertex, null)) {
                settledNodes.add(chosenVertex);
                unSettledNodes.remove(chosenVertex);
                chosenVertex.setVisited(true);
                Map<Vertex, Integer> neighbours = chosenVertex.getNeighboursWithDistances();

                for (Map.Entry<Vertex, Integer> entry : neighbours.entrySet()) {
                    if (d[entry.getKey().getId()] > d[position] + entry.getValue()) {
                        d[entry.getKey().getId()] = d[position] + entry.getValue();
                        p[entry.getKey().getId()] = position;
                    }
                }
            }
        }

        return d[stop];
    }

    private void initializeVariables() {
        vertices = new HashSet<>();
        vertices.addAll(unSettledNodes);
        settledNodes = new HashSet<>();
        d = new int[vertices.size()];
        p = new int[vertices.size()];
        d = Arrays.stream(d).map(x -> Integer.MAX_VALUE).toArray();
        p = Arrays.stream(p).map(x -> -1).toArray();
        d[start] = 0;
    }
}
